package com.codetalenta.androidjetpackmvvm.Helper;

import com.codetalenta.androidjetpackmvvm.Api.ApiService;

public class UrlApi {
    public static String BASE_URL = "https://fahrulspringrest.herokuapp.com/";

    public static ApiService getApiService(){
        return RetrofitClient.getClient(BASE_URL).create(ApiService.class);
    }
}
