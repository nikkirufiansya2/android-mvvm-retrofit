package com.codetalenta.androidjetpackmvvm.Api;

import com.codetalenta.androidjetpackmvvm.models.LoginData;
import com.codetalenta.androidjetpackmvvm.models.ModelMahasiswa;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiService {
    @POST("authenticate")
    @Headers({
            "Content-Type: application/json;charset=utf-8",
            "Accept: application/json;charset=utf-8",
            "Cache-Control: max-age=640000"
    })
    Call<ResponseBody> login(@Body LoginData body);

    @GET("api/findallmahasiswa")
    @Headers({
            "Content-Type: application/json;charset=utf-8",
            "Accept: application/json;charset=utf-8",
            "Cache-Control: max-age=640000"
    })
    Call<ResponseBody> getMahasiswa(@Header("Authorization") String auth);

    @POST("api/savemahasiswa")
    @Headers({
            "Content-Type: application/json;charset=utf-8",
            "Accept: application/json;charset=utf-8",
            "Cache-Control: max-age=640000"
    })
    Call<ResponseBody> insertMahasiswa(@Header("Authorization") String auth, @Body ModelMahasiswa mahasiswa);

    @DELETE("api/deletemahasiswa/{id}")
    @Headers({
            "Content-Type: application/json;charset=utf-8",
            "Accept: application/json;charset=utf-8",
            "Cache-Control: max-age=640000"
    })
    Call<ResponseBody> deleteMahasiswa(@Header("Authorization") String auth, @Path(value = "id") int id);

    @PUT("api/updatemahasiswa/{id}")
    @Headers({
            "Content-Type: application/json;charset=utf-8",
            "Accept: application/json;charset=utf-8",
            "Cache-Control: max-age=640000"
    })
    Call<ResponseBody> updateMahasiswa(@Header("Authorization") String auth, @Body ModelMahasiswa mahasiswa, @Path(value = "id") int id);
}
