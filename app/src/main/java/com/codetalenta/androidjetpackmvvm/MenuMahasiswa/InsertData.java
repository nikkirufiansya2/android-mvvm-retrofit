package com.codetalenta.androidjetpackmvvm.MenuMahasiswa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.codetalenta.androidjetpackmvvm.Api.ApiService;
import com.codetalenta.androidjetpackmvvm.Helper.Session;
import com.codetalenta.androidjetpackmvvm.Helper.UrlApi;
import com.codetalenta.androidjetpackmvvm.R;
import com.codetalenta.androidjetpackmvvm.databinding.ActivityInsertDataBinding;
import com.codetalenta.androidjetpackmvvm.models.ModelMahasiswa;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InsertData extends AppCompatActivity {
    ActivityInsertDataBinding binding;
    ApiService apiService = UrlApi.getApiService();
    Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_insert_data);
        session = new Session(this);
        binding.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertData();
            }
        });
    }

    private void insertData() {
        String name = binding.name.getText().toString().trim();
        String email = binding.email.getText().toString().trim();
        apiService.insertMahasiswa(session.getString("token"), new ModelMahasiswa(name,email)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
               if (response.isSuccessful()){
                   startActivity(new Intent(InsertData.this, ManuUtama.class));
               }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

}