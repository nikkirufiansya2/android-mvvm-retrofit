package com.codetalenta.androidjetpackmvvm.MenuMahasiswa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.codetalenta.androidjetpackmvvm.Api.ApiService;
import com.codetalenta.androidjetpackmvvm.Helper.Session;
import com.codetalenta.androidjetpackmvvm.Helper.UrlApi;
import com.codetalenta.androidjetpackmvvm.R;
import com.codetalenta.androidjetpackmvvm.databinding.ActivityEditDataBinding;
import com.codetalenta.androidjetpackmvvm.models.ModelMahasiswa;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditData extends AppCompatActivity {
    ActivityEditDataBinding binding;
    int id;
    String nama, email;
    ApiService apiService = UrlApi.getApiService();
    Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_data);
        session = new Session(this);
        id = getIntent().getIntExtra("id",0);
        nama = getIntent().getStringExtra("nama");
        email = getIntent().getStringExtra("email");
        binding.name.setText(nama);
        binding.email.setText(email);
        binding.update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                update();
            }
        });
        binding.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete();
            }
        });
    }

    private void update() {
        String names =  binding.name.getText().toString();
        String emails = binding.email.getText().toString();
        apiService.updateMahasiswa(session.getString("token"), new ModelMahasiswa(names, emails), id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    startActivity(new Intent(EditData.this, ManuUtama.class));
                }else {
                    Toast.makeText(EditData.this, "Update data gagal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void delete(){
        apiService.deleteMahasiswa(session.getString("token"),id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    startActivity(new Intent(EditData.this, ManuUtama.class));
                }else {
                    Toast.makeText(EditData.this, "Gagal Menghapus Data", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

}