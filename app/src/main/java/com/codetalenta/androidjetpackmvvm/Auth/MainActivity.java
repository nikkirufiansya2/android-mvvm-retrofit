package com.codetalenta.androidjetpackmvvm.Auth;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.codetalenta.androidjetpackmvvm.Api.ApiService;
import com.codetalenta.androidjetpackmvvm.Helper.Session;
import com.codetalenta.androidjetpackmvvm.Helper.UrlApi;
import com.codetalenta.androidjetpackmvvm.models.LoginData;
import com.codetalenta.androidjetpackmvvm.MenuMahasiswa.ManuUtama;
import com.codetalenta.androidjetpackmvvm.R;
import com.codetalenta.androidjetpackmvvm.databinding.ActivityMainBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    ActivityMainBinding binding;
    ApiService apiService = UrlApi.getApiService();
    Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        session = new Session(this);
        if (session.getBoolean("login")){
            startActivity(new Intent(MainActivity.this, ManuUtama.class));
        }
        binding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    private void login() {
        String username = binding.username.getText().toString();
        String password = binding.password.getText().toString();
        apiService.login(new LoginData(username, password)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    if (response.isSuccessful()) {
                        try {
                            String respon = response.body().string();
                            JSONObject jsonObject = new JSONObject(respon);
                            String token = jsonObject.getString("token");
                            session.add("token", "Bearer" + " " + token);
                            session.add("login", true);
                            startActivity(new Intent(MainActivity.this, ManuUtama.class));
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else if (response.code() == 400) {
                    System.out.println("Bad request");
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }
}