package com.codetalenta.androidjetpackmvvm.MenuMahasiswa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.codetalenta.androidjetpackmvvm.Adapter.AdapterMahasiswa;
import com.codetalenta.androidjetpackmvvm.Api.ApiService;

import com.codetalenta.androidjetpackmvvm.Helper.Session;
import com.codetalenta.androidjetpackmvvm.Helper.UrlApi;
import com.codetalenta.androidjetpackmvvm.R;
import com.codetalenta.androidjetpackmvvm.databinding.ActivityManuUtamaBinding;
import com.codetalenta.androidjetpackmvvm.models.ModelMahasiswa;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ManuUtama extends AppCompatActivity {
    ActivityManuUtamaBinding binding;
    Session session;
    List<ModelMahasiswa> modelMahasiswas = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_manu_utama);
        session = new Session(this);
        getData();
        swipeRefresh();
        btnAdd();
    }

    private void btnAdd() {
        binding.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ManuUtama.this, InsertData.class));
            }
        });
    }


    private void swipeRefresh() {
        binding.refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItem();
            }
            void refreshItem() {
                getData();
                onItemLoad();
            }
            void onItemLoad() {
                binding.refresh.setRefreshing(false);
            }
        });
    }

    private void getData() {
        ApiService apiService = UrlApi.getApiService();
        apiService.getMahasiswa(session.getString("token")).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String respon = response.body().string();
                    JSONArray jsonArray = new JSONArray(respon);
                    modelMahasiswas.clear();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        modelMahasiswas.add(new ModelMahasiswa(object.getInt("id"), object.getString("nama"), object.getString("email")));
                    }
                    AdapterMahasiswa adapterMahasiswa = new AdapterMahasiswa(ManuUtama.this, modelMahasiswas);
                    binding.recyclerView.setLayoutManager(new GridLayoutManager(ManuUtama.this, 2));
                    binding.recyclerView.setAdapter(adapterMahasiswa);
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

}