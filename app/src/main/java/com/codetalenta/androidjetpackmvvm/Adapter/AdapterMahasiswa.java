package com.codetalenta.androidjetpackmvvm.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codetalenta.androidjetpackmvvm.MenuMahasiswa.EditData;
import com.codetalenta.androidjetpackmvvm.databinding.ListItemBinding;
import com.codetalenta.androidjetpackmvvm.models.ModelMahasiswa;

import java.util.List;

public class AdapterMahasiswa extends RecyclerView.Adapter<AdapterMahasiswa.MyView> {
    Context context;
    List<ModelMahasiswa> data;

    public AdapterMahasiswa(Context context, List<ModelMahasiswa> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public AdapterMahasiswa.MyView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ListItemBinding listItemBinding = ListItemBinding.inflate(layoutInflater, parent, false);
        return new MyView(listItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterMahasiswa.MyView holder, int position) {
        ModelMahasiswa modelMahasiswa = data.get(position);
        holder.bind(modelMahasiswa);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyView extends RecyclerView.ViewHolder {
        ListItemBinding listItemBinding;
        public MyView(ListItemBinding binding) {
            super(binding.getRoot());
            this.listItemBinding = binding;
            listItemBinding.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int id = binding.getMahasiswa().getId();
                    String nama = binding.getMahasiswa().getNama();
                    String email = binding.getMahasiswa().getEmail();
                    Intent intent = new Intent(context, EditData.class);
                    intent.putExtra("id",id);
                    intent.putExtra("nama", nama);
                    intent.putExtra("email", email);
                    context.startActivity(intent);
                }
            });

        }

        public void bind(ModelMahasiswa modelMahasiswa) {
            listItemBinding.setMahasiswa(modelMahasiswa);
            listItemBinding.executePendingBindings();
        }

    }
}
